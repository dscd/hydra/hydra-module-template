[(Français)](#le-nom-du-projet)

## Hydra Module Cookiecuttter

A ``cookiecutter`` template for ```dsd.hydra.module``` within Statistics Canada and part of the Hydra Platform. The goal is to reduce the amount of set up tasks associated with starting Hydra Module projects at Statistics Canada.

This template helps to set up standardized project, and includes security features using pre-commit hooks.

### How to Contribute

See [CONTRIBUTING.md](CONTRIBUTING.md)

### License

Unless otherwise noted, the source code of this project is covered under Crown Copyright, Government of Canada, and is distributed under the [MIT License](LICENSE).

The Canada wordmark and related graphics associated with this distribution are protected under trademark law and copyright law. No permission is granted to use them outside the parameters of the Government of Canada's corporate identity program. For more information, see [Federal identity requirements](https://www.canada.ca/en/treasury-board-secretariat/topics/government-communications/federal-identity-requirements.html).

______________________

## Le nom du projet

Un gabarit ``cookiecutter`` pour ```dsd.hydra.module``` au sein de Statistique Canada et faisant partie de la plateforme Hydra. L'objectif est de réduire le nombre de tâches de configuration associées au démarrage des projets du module Hydra à Statistique Canada.


Ce modèle aide à mettre en place un projet standardisé et comprend des fonctions de sécurité utilisant des crochets de pré-commit.

### Comment contribuer

Voir [CONTRIBUTING.md](CONTRIBUTING.md)

### Licence

Sauf indication contraire, le code source de ce projet est protégé par le droit d'auteur de la Couronne du gouvernement du Canada et distribué sous la [licence MIT](LICENSE).

Le mot-symbole « Canada » et les éléments graphiques connexes liés à cette distribution sont protégés en vertu des lois portant sur les marques de commerce et le droit d'auteur. Aucune autorisation n'est accordée pour leur utilisation à l'extérieur des paramètres du programme de coordination de l'image de marque du gouvernement du Canada. Pour obtenir davantage de renseignements à ce sujet, veuillez consulter les [Exigences pour l'image de marque](https://www.canada.ca/fr/secretariat-conseil-tresor/sujets/communications-gouvernementales/exigences-image-marque.html).

#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -----------------------------------------------------------
# Testing Hydra Module Template
#
# (C) 2022 Statistics Canada
# Author: Andres Solis Montero
# -----------------------------------------------------------
import os
import pytest
from py._path.local import LocalPath
import tempfile
import mimetypes as mt
from pydantic import BaseModel
from os.path import basename, exists
from typing import Optional
from fastapi.testclient import TestClient
from dsd.hydra.module.{{ cookiecutter.package_name }}.main import app
from dsd.hydra.module.{{ cookiecutter.package_name }}.models.schemas import (
    Selection, 
    Arguments,
    Annotation,
    Extraction,
    Redaction,
    StatusOK
)

from conftest import checksum, UploadTest, HealthTest, ActionTest




upload_tests = [
    UploadTest(
        files=["blank.pdf", "blank.pdf"], exp_status_code=200
    ),  # Multiple files allowed
    UploadTest(files=["blank.png", "blank.jpg"], exp_status_code=200),
    UploadTest(files=["blank.txt"], exp_status_code=418),  # File not Allowed
    UploadTest(files=["none"], exp_status_code=422),  # File not found
]

health_tests = [
    HealthTest(route="/readyz", exp_status_code=200, exp_response=StatusOK()),
    HealthTest(route="/healthz", exp_status_code=200, exp_response=StatusOK()),
    HealthTest(route="/livez", exp_status_code=200, exp_response=StatusOK()),
]

action_tests = [
    ActionTest(
        route="/extraction_method",
        filename="blank.pdf",
        selection=Selection(areas=[]),
        args=Arguments(),
        exp_status_code=200,
        exp_response=Extraction(org_document="", extracted=[]),
    ),
    ActionTest(
        route="/annotation_method",
        filename="blank.pdf",
        selection=Selection(areas=[]),
        args=None,
        exp_status_code=200,
        exp_response=Annotation(org_document="", areas=[]),
    ),
    ActionTest(
        route="/redaction_method",
        filename="blank.pdf",
        selection=None,
        args=None,
        exp_status_code=200,
        exp_response=Redaction(org_document="", new_document=""),
    ),
]



client = TestClient(app)


@pytest.mark.kubernetes
@pytest.mark.parametrize(
    "path, exp_status_code, exp_response",
    [(u.route, u.exp_status_code, u.exp_response) for u in health_tests],
)
def test_health(path: str, exp_status_code: int, exp_response: dict):
    """
    Parametrizable test to check Kubernetes pods health
    Args:
        path (str): Path FastAPI route
        exp_status_code (int): HTTP status_code response of accesing path route
        exp_response (dict): Body response object
    """
    response = client.get(path)
    assert response.status_code == exp_status_code
    assert response.json() == exp_response


@pytest.mark.upload
@pytest.mark.parametrize(
    "files, exp_status_code", [(u.files, u.exp_status_code) for u in upload_tests]
)
def test_upload_download_delete(files: list, exp_status_code: int, datadir: LocalPath):
    """Testing the cycle of upload,download, and deleting a list of files.

    Args:
        files (list): a list of filenames
        exp_status_code (int): return code
    """
    files = [datadir.join(f) for f in files]
    tmp_local_file = datadir.join(next(tempfile._get_candidate_names()))

    u_checksum = [checksum(f) for f in files if exists(f)]
    files = [
        ("file", (basename(f), open(f, "rb"), mt.guess_type(f)[0]))
        for f in files
        if exists(f)
    ]

    response = client.post("/upload", files=files)
    assert response.status_code == exp_status_code
    # Only try to download and delete if the upload was
    # sucessfull
    if 200 == exp_status_code:
        for idx, f in enumerate(response.json()):
            json_request = {"filename": f}
            rp = client.post("/download", json=json_request)
            assert rp.status_code == exp_status_code

            # If a sucessfull file was downloaded
            # check its checksum to validate it's the same file
            if 200 == exp_status_code:
                with open(tmp_local_file, "wb") as fh:
                    fh.write(rp.content)
                assert u_checksum[idx] == checksum(tmp_local_file)
                os.remove(tmp_local_file)

            rp = client.post("/delete", json=json_request)
            assert rp.status_code == exp_status_code
            # only if a file was deleted successfuly, check for the response
            if 200 == exp_status_code:
                assert rp.json() == StatusOK()


@pytest.mark.action
@pytest.mark.parametrize(
    "route, filename, selection, args, exp_status_code, exp_response",
    [
        (a.route, a.filename, a.selection, a.args, a.exp_status_code, a.exp_response)
        for a in action_tests
    ],
)
def test_action(
    route: str,
    filename: str,
    selection: Optional[Selection],
    args: Optional[Arguments],
    exp_status_code: int,
    exp_response: BaseModel,
    datadir: LocalPath,
):
    filename = datadir.join(filename)
    files = [
        ("file", (basename(filename), open(filename, "rb"), mt.guess_type(filename)[0]))
    ]
    response = client.post("/upload", files=files)
    assert 200 == response.status_code
    uploaded_filename = response.json()[0]
    try:
        json_request = {
            "filename": uploaded_filename,
            "selection": selection.dict() if selection is not None else None,
            "args": args.dict() if args is not None else None,
        }
        action_rp = client.post(route, json=json_request)
        assert exp_status_code == action_rp.status_code
        exp_response.org_document = uploaded_filename
        assert action_rp.json() == exp_response
    finally:
        json_request = {"filename": uploaded_filename}
        rp = client.post("/delete", json=json_request)
        assert 200 == rp.status_code


def test_routes_docstring_not_empty():
    """
    Search all FastAPI routes and checks if
    their docstring are not empty.
    Fails if a route hasn't be documented
    """
    routes = app.openapi()["paths"].keys()
    for route in app.routes:
        if route.path in routes:
            assert route.description != ""


def test_coverage():
    """
    Test that all FastAPI routes have at least a test_case.
    """
    routes = app.openapi()["paths"].keys()
    actions = set([a.route for a in action_tests])
    health = set([h.route for h in health_tests])
    upload = set(["/upload", "/delete", "/download"])
    diff = set(routes) - (set(actions) | set(health) | set(upload))

    
    assert 0 == len(diff)

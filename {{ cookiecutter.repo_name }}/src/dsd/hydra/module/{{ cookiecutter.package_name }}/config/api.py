#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -----------------------------------------------------------
# Hydra Module Template Config
#
# (C) 2022 Statistics Canada
# Author: Andres Solis Montero
# -----------------------------------------------------------

import yaml
from enum import Enum
from functools import lru_cache
from pydantic import BaseSettings, Field
from os.path import dirname, join, realpath
from collections import namedtuple
from typing import NamedTuple


class ConfigFile(Enum):
    """
    Enumerator containig all Configuration files
    """

    environment = ".env"
    config = "config.yml"


class Settings(BaseSettings):
    """
    Necesary enviroment variables needed for authentication.
    If these variables are not set, any file including this
    module will stop executing.
    """

    host: str = Field(env="UVICORN_HOST", default="127.0.0.1")
    port: int = Field(env="UVICORN_PORT", default=8000)
    reload: bool = Field(env="UVICORN_RELOAD", default=True)
    debug: bool = Field(env="UVICORN_DEBUG", default=True)
    workers: int = Field(env="UVICORN_WORKERS", default=2)

    docs_url: str = Field(env="FASTAPI_DOCS_URL", default="/docs")
    openai_url: str = Field(env="FASTAPI_OPENAI_URL", default="/v2/openapi.json")
    nb_prefix: str = Field(env="NB_PREFIX", default="")


    def root_path(self):
        """
        Provides uvicorn root path if necessary. Automatically checks 
        if AAW NB_PREFIX variable is set to set the root path to aaw prefix path. 
        If AAW NB_PREFIX isn't present, then the empty string will be returned.
        No path (i.e., empty string) is the default behaviour of root_path. 
        """
        return f"{self.nb_prefix}/proxy/{self.port}/" if self.nb_prefix else ''
        
    class Config:
        """
        Provides the path to a ".env" containing a list of environments
        variables to be set in case that they don't exist yet.
        """

        env_file: str = ConfigFile.environment.value
        env_file_encoding: str = "utf-8"


settings = Settings()


@lru_cache
def config() -> NamedTuple:
    """
    Returns a python dictionary with specified configurations only once.
    Returns the same instance after first call.
    """
    data = {}
    directory = dirname(realpath(__file__))
    configfile = join(directory, ConfigFile.config.value)
    with open(configfile, "r") as stream:
        try:
            data = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)
    return namedtuple("Config", data.keys())(*data.values())

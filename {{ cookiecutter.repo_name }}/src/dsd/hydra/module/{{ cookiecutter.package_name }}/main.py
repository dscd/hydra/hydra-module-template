#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -----------------------------------------------------------
# Hydra Module Template
#
# (C) 2022 Statistics Canada
# Author: Andres Solis Montero
# -----------------------------------------------------------

import os
import shutil
import mimetypes as mt
from os.path import basename, exists
from typing import List, Optional
from tempfile import NamedTemporaryFile
from fastapi import FastAPI, File, UploadFile, HTTPException, Body
from fastapi.responses import FileResponse
from dsd.hydra.module.{{ cookiecutter.package_name }}.config.api import settings, config
from dsd.hydra.module.{{ cookiecutter.package_name }}.models.schemas import (
    Selection,
    Annotation,
    Extraction,
    Redaction,
    Arguments,
    StatusOK,
)

app = FastAPI(docs_url=settings.docs_url, openai_url=settings.openai_url)


@app.post("/upload")
async def upload(file: List[UploadFile] = File(...)) -> List[str]:
    """Upload a list of files to the RestAPI local storage


    Args:
        file (List[UploadFile]): List of stream files. Defaults to File(...).

    Raises:
        HTTPException: _description_

    Returns:
        List[str]: Return a list of the local filenames in the same order as uploaded.
        The local filenames will serve as a ID for future processing, downloading,
        and deleting local files.
    """

    paths = []
    _mtypes = config().mimetypes

    for f in file:
        filename = basename(f.filename)
        content_type = f.content_type if f.content_type else mt.guess_type(filename)[0]

        if content_type not in _mtypes:
            raise HTTPException(status_code=418, detail="File not allowed!")
        try:
            with NamedTemporaryFile(delete=False, suffix=f"_{filename}") as tmp:
                shutil.copyfileobj(f.file, tmp)
                paths.append(tmp.name)
        finally:
            f.file.close()
    return paths


@app.post("/download")
async def download(filename: str = Body(..., embed=True)) -> FileResponse:
    """Download the local file.

    Args:
        filename (str): local file path. Defaults to Body(..., embed=True).

    Raises:
        HTTPException: raise an exception if file doesn't exist

    Returns:
        FileResponse: return a file response with the content of the file
    """
    if not exists(filename):
        raise HTTPException(status_code=404, detail="File not found!")
    return FileResponse(filename)


@app.post("/delete")
async def delete(filename: str = Body(..., embed=True)) -> StatusOK:
    """Deletes a local file.
    Invoked by the Cache API to clean uploaded files after an action was invoked


    Args:
        filename (str): path of the interal RestAPI storage.
                                  Defaults to Body(..., embed=True).

    Raises:
        HTTPException: raises a 404 status code if filename doesn't exist

    Returns:
        StatusOK: returns a status ok response if file is successfuly removed
    """
    if not exists(filename):
        raise HTTPException(status_code=404, detail="File not found!")
    else:
        os.remove(filename)
    return StatusOK()


@app.post("/extraction_method")
async def action_extraction_method(
    filename: str = Body(..., embed=True),
    selection: Optional[Selection] = None,
    args: Optional[Arguments] = None,
) -> Extraction:
    """Example of an extraction method

    Args:
        filename (str, optional): filename path of the interal RestAPI storage.
                Defaults to Body(..., embed=True).
        selection (Optional[Selection], optional): selection object specifying which
                part of the document to process. A value of None represents the whole
                document. Defaults to None.
        args (Optional[Arguments], optional): extra arguments for the extraction method.
                Defaults to None.

    Returns:
        Extraction: returns an Extraction object
    """
    return Extraction(org_document=filename, extracted=[])


@app.post("/annotation_method")
async def action_annotation_method(
    filename: str = Body(..., embed=True),
    selection: Optional[Selection] = None,
    args: Optional[Arguments] = None,
) -> Annotation:
    """Example of an annotation method

    Args:
        filename (str, optional): filename path of the interal RestAPI storage.
                Defaults to Body(..., embed=True).
        selection (Optional[Selection], optional): selection object specifying which
                part of the document to process. A value of None represents the whole
                document. Defaults to None.
        args (Optional[Arguments], optional): extra arguments for the extraction method.
                Defaults to None.

    Returns:
        Annotation: returns an Annotation object
    """
    return Annotation(org_document=filename, areas=[])


@app.post("/redaction_method")
async def action_redation_method(
    filename: str = Body(..., embed=True),
    selection: Optional[Selection] = None,
    args: Optional[Arguments] = None,
) -> Redaction:
    """Example of an redaction method

    Args:
        filename (str, optional): filename path of the interal RestAPI storage.
                Defaults to Body(..., embed=True).
        selection (Optional[Selection], optional): selection object specifying which
                part of the document to process. A value of None represents the whole
                document. Defaults to None.
        args (Optional[Arguments], optional): extra arguments for the extraction method.
                Defaults to None.

    Returns:
        Redaction: Returns an Redaction object
    """
    return Redaction(org_document=filename, new_document="")


@app.get("/healthz", status_code=200)
def healthz() -> StatusOK:
    """Healthcheck endpoint for kubernetes.

    Returns:
        StatusOK: returns an status ok object
    """
    return StatusOK()


@app.get("/readyz", status_code=200)
def readyz() -> StatusOK:
    """Healthcheck endpoint for kubernetes.

    Returns:
        StatusOK: returns an status ok object
    """
    return StatusOK()


@app.get("/livez", status_code=200)
def livez() -> StatusOK:
    """Healthcheck endpoint for kubernetes.

    Returns:
        StatusOK: returns an status ok object
    """
    return StatusOK()

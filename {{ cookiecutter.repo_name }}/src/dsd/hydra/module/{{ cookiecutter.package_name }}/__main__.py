#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -----------------------------------------------------------
# Hydra Module Template
#
# (C) 2022 Statistics Canada
# Author: Andres Solis Montero
# -----------------------------------------------------------
import sys
import pprint
import uvicorn
from dsd.hydra.module.{{ cookiecutter.package_name }}.config.api import settings


def main():
    pprint.pprint(settings.dict(), width=1)
    uvicorn.run(
        "dsd.hydra.module.{{ cookiecutter.package_name }}.main:app",
        host=settings.host,
        port=settings.port,
        reload=settings.reload,
        debug=settings.reload,
        workers=settings.workers,
        root_path=settings.root_path()
    )


if __name__ == "__main__":
    sys.exit(main())

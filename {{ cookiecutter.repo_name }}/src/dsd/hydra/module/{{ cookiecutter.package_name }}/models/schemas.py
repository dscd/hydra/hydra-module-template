#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -----------------------------------------------------------
# Hydra Module Template Models
#
# (C) 2022 Statistics Canada
# Author: Andres Solis Montero
# -----------------------------------------------------------
from pydantic import BaseModel, Field
from typing import Optional, List


class StatusOK(BaseModel):
    """Basic HTTP Status Response"""

    status: str = "OK"


class Region(BaseModel):
    """Rectangular document region

    Defines a rectangular document region for a pdf or image.
    The region is specified as the top-left corner plus width and height

    Attributes:
        x (:obj:`int`): x coordinates of the rectangle top left corner.
        y (:obj:`int`): y coordinate of the rectangle top left corner.
        w (:obj:`int`): rectangle width
        h (:obj:`int`): rectangle height

    """

    x: int
    y: int
    w: int
    h: int


class PageRegion(BaseModel):
    """Paged document region

    Defines a document page or a rectangular region of a page for a pdf or image.
    The region is specified as the top-left corner plus width and height.
    If no region is specified the whole page is assumed as the target.

    Attributes:
        page (:obj:`int`): 0-based page number
        region (:obj:`Region`, optional): Optional rectangular region,
                    if None the whole page is assumed the as the target.

    """

    page: int
    region: Optional[Region] = None


class Metadata(BaseModel):
    """Defines a key value pair

    Attributes:
        key (:obj:`str`): key name
        value (:obj:`dict`): generic python dict

    """

    key: str
    value: dict


class RegionMetadata(PageRegion):
    """Adds a list of metadata to a PageRegion

    See :obj:`PageRegion` for other attributes

    Attributes:
        meta (:obj:`List[Metadata]`): list of key value pairs associated to a region
    """

    meta: List[Metadata] = Field(default_factory=list)


class Selection(BaseModel):
    """Defines which areas of a document should be processed.


    Attributes:
        areas (:obj:`List[PageRegion]`): list of page regions
    """

    areas: List[PageRegion]


class Annotation(BaseModel):
    """Annotation output object defines a list of annotated (RegionMetadata)
    from the original document.

    Attributes:
        org_document (:obj:`str`): local filename
        areas(:obj:`List[RegionMetadata]`): list of annotations
    """

    org_document: str
    areas: List[RegionMetadata]


class Extraction(BaseModel):
    """Extraction output object defines a list of extracted (RegionMetadata)
    from the original document.

    Attributes:
        org_document (:obj:`str`): local filename
        areas(:obj:`List[RegionMetadata]`): list of annotations
    """

    org_document: str
    extracted: List[RegionMetadata]


class Redaction(BaseModel):
    """Creates a new version of the original document.
    Specifies the path to the original and new document.

    Attributes:
        org_document (:obj:`str`): local filename
        areas(:obj:`List[RegionMetadata]`): list of annotations
    """

    org_document: str
    new_document: str


class Arguments(BaseModel):
    """
    Base Class for API arguments
    """
